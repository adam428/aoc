day10:
	go test ./2019/day10/... && go run 2019/day10/main.go

day9:
	go test ./2019/day9/... && go run 2019/day9/main.go

day8:
	go test ./2019/day8/... && go run 2019/day8/main.go && open image.png

day7:
	go test ./2019/day7/... && go run 2019/day7/main.go

day6:
	go test ./... && go run 2019/day6/main.go

day5:
	go test ./... && go run 2019/day5/main.go

day4:
	go test ./... && go run 2019/day4/main.go

day3:
	go test ./... && go run 2019/day3/main.go

day2:
	go test ./... && go run 2019/day2/main.go

day1:
	go test ./... && go run 2019/day1/main.go

