module github.com/AdamFromTumelo/aoc

go 1.21.0

require (
	github.com/gammazero/deque v0.2.1
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d
	gopkg.in/yaml.v3 v3.0.1 // indirect
	gotest.tools v2.2.0+incompatible
)
