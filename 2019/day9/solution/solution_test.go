package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_Solution2(t *testing.T) {
	tts := []struct {
		name   string
		code   string
		result string
	}{}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			result := SolvePart1([]string{tt.code})
			require.Equal(t, tt.result, result)
		})
	}
}
