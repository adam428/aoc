package solution

import (
	"log"

	"github.com/AdamFromTumelo/aoc/2019/day9/intcode"
)

func SolvePart1(lines []string) string {
	boostProgram := intcode.InitMemory(lines[0])

	cpu := intcode.InitProcessor(boostProgram)
	cpu.EnterInput(1)
	cpu.Run()

	output := (cpu.PopOutput())
	log.Println(">>>>>>> ", output)

	return output
}

func SolvePart2(lines []string) string {
	boostProgram := intcode.InitMemory(lines[0])

	cpu := intcode.InitProcessor(boostProgram)
	cpu.EnterInput(2)
	cpu.Run()

	output := (cpu.PopOutput())
	log.Println(">>>>>>> ", output)

	return output
}
