package intcode

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

type Memory []int

type Processor struct {
	IP     int
	Memory Memory
	Cycles int

	Input  string
	Output string

	DieOnDump bool
	Halted    bool

	RelativeBaseOffset int
}

const MAX_CYCLES = 1_000_000

type Opcode int

const (
	ADD    Opcode = 1
	MUL    Opcode = 2
	INPUT  Opcode = 3
	OUTPUT Opcode = 4
	JMPT   Opcode = 5
	JMPF   Opcode = 6
	LT     Opcode = 7
	EQ     Opcode = 8
	RBO    Opcode = 9
	HCF    Opcode = 99
	NIL    Opcode = 0
)

type ParameterMode int

const (
	PositionMode  ParameterMode = 0
	ImmediateMode ParameterMode = 1
	RelativeMode  ParameterMode = 2
)

type Instruction struct {
	Opcode         Opcode
	Size           int
	ParameterModes []ParameterMode
}

func InitProcessor(program Memory) Processor {
	var local Memory = make(Memory, len(program))
	copy(local, program)

	return Processor{
		IP:                 0,
		Memory:             local,
		Cycles:             0,
		Input:              "",
		Output:             "",
		DieOnDump:          false,
		Halted:             false,
		RelativeBaseOffset: 0,
	}
}

func InitMemory(line string) (program Memory) {
	opcodesStr := strings.Split(line, ",")
	program = make([]int, len(opcodesStr))

	for i, opcodeStr := range opcodesStr {
		opcode, err := strconv.Atoi(opcodeStr)
		if err != nil {
			log.Fatalf("init program: %s", err)
		}
		program[i] = opcode
	}

	return
}

func (cpu *Processor) Run() {
	for {
		cpu.Cycles += 1
		if cpu.Cycles > MAX_CYCLES {
			log.Fatalf("max cycles reached!!")
		}

		instruction := cpu.decode(cpu.Memory[cpu.IP])

		if instruction.Opcode == HCF {
			cpu.Halted = true
			return
		}

		oldIP := cpu.IP
		yield := cpu.execute(instruction)
		if yield {
			return
		}

		// if the instruction modifies the instruction pointer,
		// that value is used and the instruction pointer is not automatically increased.
		if cpu.IP == oldIP {
			cpu.IP += instruction.Size
		}
	}
}

func decode(opcode Opcode) Instruction {
	switch opcode {
	case ADD:
		return Instruction{Opcode: ADD, Size: 4}
	case MUL:
		return Instruction{Opcode: MUL, Size: 4}
	case INPUT:
		return Instruction{Opcode: INPUT, Size: 2}
	case OUTPUT:
		return Instruction{Opcode: OUTPUT, Size: 2}
	case JMPT:
		return Instruction{Opcode: JMPT, Size: 3}
	case JMPF:
		return Instruction{Opcode: JMPF, Size: 3}
	case LT:
		return Instruction{Opcode: LT, Size: 4}
	case EQ:
		return Instruction{Opcode: EQ, Size: 4}
	case RBO:
		return Instruction{Opcode: RBO, Size: 2}
	case HCF:
		return Instruction{Opcode: HCF, Size: 1}
	}

	log.Fatalf("decoder: unknown opcode %d", opcode)
	return Instruction{}
}

func (cpu *Processor) MemoryRead(address int) int {
	cpu.extendMemoryUpTo(address)
	return cpu.Memory[address]
}

func (cpu *Processor) MemoryWrite(address, value int) {
	cpu.extendMemoryUpTo(address)
	cpu.Memory[address] = value
}

func (cpu *Processor) extendMemoryUpTo(address int) {
	if len(cpu.Memory) < address+1 {
		n := address - len(cpu.Memory) + 1
		cpu.Memory = append(cpu.Memory, make([]int, n)...)
	}
}

func (cpu *Processor) EnterInput(input int) {
	if cpu.Input != "" {
		cpu.Input = strings.Join([]string{cpu.Input, fmt.Sprintf("%d", input)}, "\n")
	} else {
		cpu.Input = strings.Join([]string{fmt.Sprintf("%d", input)}, "\n")
	}
}

func (cpu *Processor) PopOutput() string {
	output := cpu.Output
	cpu.Output = ""

	return output
}

func (cpu *Processor) decode(code int) Instruction {
	opcode := code % 100
	instruction := decode(Opcode(opcode))

	instruction = cpu.decodeParameterModes(code, instruction)
	return instruction
}

// ABCDE
// 01002
//
// DE - two-digit opcode,      02 == opcode 2
// C - mode of 1st parameter,  0 == position mode
// B - mode of 2nd parameter,  1 == immediate mode
// A - mode of 3rd parameter,  0 == position mode, omitted due to being a leading zero
func (cpu *Processor) decodeParameterModes(code int, instruction Instruction) Instruction {
	instruction.ParameterModes = make([]ParameterMode, instruction.Size-1)         //take 1 off for the opcode
	codeStr := fmt.Sprintf("%0"+fmt.Sprintf("%d", (instruction.Size+1))+"d", code) //pad with leading zeros to get a string of length opcode+num of parameters

	for i := 0; i < instruction.Size-1; i += 1 {
		currentParameterModeStr := string(codeStr[instruction.Size-2-i])
		switch string(currentParameterModeStr) {
		case "0":
			instruction.ParameterModes[i] = PositionMode
		case "1":
			instruction.ParameterModes[i] = ImmediateMode
		case "2":
			instruction.ParameterModes[i] = RelativeMode
		default:
			log.Fatalf("trouble decoding this parameter mode: %d", code)
		}
	}

	return instruction
}

func (cpu *Processor) execute(instruction Instruction) (yielding bool) {
	yielding = false
	switch instruction.Opcode {
	case ADD:
		cpu.execAdd(instruction)
	case MUL:
		cpu.execMul(instruction)
	case INPUT:
		yielding = cpu.execInput(instruction)
	case OUTPUT:
		cpu.execOutput(instruction)
	case JMPT:
		cpu.execJumpIfTrue(instruction)
	case JMPF:
		cpu.execJumpIfFalse(instruction)
	case LT:
		cpu.execLessThan(instruction)
	case EQ:
		cpu.execEqual(instruction)
	case RBO:
		cpu.execRelativeBaseOffset(instruction)
	}
	return yielding
}

func (cpu *Processor) storeValue(instruction Instruction, position, value int) {
	mode := instruction.ParameterModes[position-1]
	switch mode {
	case PositionMode:
		cpu.MemoryWrite(cpu.MemoryRead(cpu.IP+position), value)
	case ImmediateMode:
		cpu.MemoryWrite(cpu.IP+position, value)
	case RelativeMode:
		cpu.MemoryWrite(cpu.RelativeBaseOffset+cpu.MemoryRead(cpu.IP+position), value)
	default:
		log.Fatalf("storeValue: invalid parameter mode: %v", mode)
	}
}
func (cpu *Processor) loadValue(instruction Instruction, position int) int {
	mode := instruction.ParameterModes[position-1]
	switch mode {
	case PositionMode:
		return cpu.MemoryRead(cpu.MemoryRead(cpu.IP + position))
	case ImmediateMode:
		return cpu.MemoryRead(cpu.IP + position)
	case RelativeMode:
		return cpu.MemoryRead(cpu.RelativeBaseOffset + cpu.MemoryRead(cpu.IP+position))
	default:
		log.Fatalf("loadValue: invalid parameter mode: %v", mode)
		return -99
	}
}

func (cpu *Processor) execAdd(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	cpu.storeValue(instruction, 3, left+right)
}

func (cpu *Processor) execMul(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	cpu.storeValue(instruction, 3, left*right)
}

func (cpu *Processor) execInput(instruction Instruction) bool {
	value, block := cpu.readFromInput()
	if !block {
		cpu.storeValue(instruction, 1, value)
	}

	return block
}

func (cpu *Processor) execOutput(instruction Instruction) {
	v := cpu.loadValue(instruction, 1)
	cpu.writeToOutput(v)
}

// Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the
// instruction pointer to the value from the second parameter.
// Otherwise, it does nothing.
func (cpu *Processor) execJumpIfTrue(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	if left != 0 {
		cpu.IP = right
	}
}

// Opcode 6 is jump-if-false: if the first parameter is zero, it sets the
// instruction pointer to the value from the second parameter.
// Otherwise, it does nothing.
func (cpu *Processor) execJumpIfFalse(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	if left == 0 {
		cpu.IP = right
	}
}

// Opcode 7 is less than: if the first parameter is less than the second
// parameter, it stores 1 in the position given by the third parameter.
// Otherwise, it stores 0.
func (cpu *Processor) execLessThan(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	var result int
	if left < right {
		result = 1
	} else {
		result = 0
	}
	cpu.storeValue(instruction, 3, result)
}

// Opcode 8 is equals: if the first parameter is equal to the second parameter,
// it stores 1 in the position given by the third parameter.
// Otherwise, it stores 0.
func (cpu *Processor) execEqual(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	var result int
	if left == right {
		result = 1
	} else {
		result = 0
	}
	cpu.storeValue(instruction, 3, result)
}

// Opcode 9 is Relative Base Offset: Opcode 9 adjusts the relative base by the
// value of its only parameter. The relative base increases (or decreases, if
// the value is negative) by the value of the parameter.
func (cpu *Processor) execRelativeBaseOffset(instruction Instruction) {
	cpu.RelativeBaseOffset += cpu.loadValue(instruction, 1)
}

func (cpu *Processor) readFromInput() (value int, block bool) {
	lines := strings.SplitN(cpu.Input, "\n", 2)
	var line, remainder string

	if len(lines) > 1 {
		line, remainder = lines[0], lines[1]
	} else {
		line, remainder = lines[0], ""
	}

	if line == "" {
		return 0, true
	}

	cpu.Input = remainder
	n, err := strconv.Atoi(line)
	if err != nil {
		log.Fatalf("readFromInput: %v", err)
	}

	return n, false
}

func (cpu *Processor) writeToOutput(value int) {
	if cpu.Output == "" {
		cpu.Output = fmt.Sprintf("%d", value)
	} else {
		cpu.Output = fmt.Sprintf("%s,%d", cpu.Output, value)
	}
}
