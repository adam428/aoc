package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_solution(t *testing.T) {

	tests := []struct {
		Name          string
		Code          string
		FinishedState []int
	}{
		{
			Name:          "simple",
			Code:          "1,0,0,0,99",
			FinishedState: []int{2, 0, 0, 0, 99},
		},
		{
			Name:          "simple 2",
			Code:          "2,3,0,3,99",
			FinishedState: []int{2, 3, 0, 6, 99},
		},
		{
			Name:          "not so simple",
			Code:          "2,4,4,5,99,0",
			FinishedState: []int{2, 4, 4, 5, 99, 9801},
		},
		{
			Name:          "last",
			Code:          "1,1,1,4,99,5,6,0,99",
			FinishedState: []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
	}
	for _, testCase := range tests {
		memory := initMemory(testCase.Code)
		processor := initProcessor(memory)
		processor.Run()
		got := processor.Memory
		want := testCase.FinishedState
		require.Equal(t, []int(want), []int(got))
	}
}
