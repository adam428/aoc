package solution

import (
	"log"
	"strconv"
	"strings"
)

func SolvePart1(lines []string) {
	program := initMemory(lines[0])
	override1202ProgramAlarm(program)
	processor := initProcessor(program)
	processor.Run()
	log.Println(" >>>>> solution: ", processor.Memory[0])
}

func override1202ProgramAlarm(memory Memory) {
	memory[1], memory[2] = 12, 2
}

func SolvePart2(lines []string) {
	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			memory := initMemory(lines[0])
			insertNounAndVerb(noun, verb, memory)
			processor := initProcessor(memory)
			processor.Run()
			if processor.Memory[0] == 19690720 {

				log.Println(" >>>>> solution: ", noun, verb)
			}
		}
	}
}

type Memory []int

type Processor struct {
	IP     int
	Memory Memory
	Cycles int
}

const MAX_CYCLES = 1_000_000

type Opcode int

const (
	ADD Opcode = iota
	MUL
	HCF // halt
	NIL
)

type Instruction struct {
	Opcode Opcode
	Size   int
}

func InstructionAdd() Instruction      { return Instruction{Opcode: ADD, Size: 4} }
func InstructionMultiply() Instruction { return Instruction{Opcode: MUL, Size: 4} }
func InstructionHalt() Instruction     { return Instruction{Opcode: HCF, Size: 1} }

func initProcessor(program Memory) Processor {
	return Processor{Memory: program, IP: 0}
}

func initMemory(line string) (program Memory) {
	opcodesStr := strings.Split(line, ",")
	program = make([]int, len(opcodesStr))

	for i, opcodeStr := range opcodesStr {
		opcode, err := strconv.Atoi(opcodeStr)
		if err != nil {
			log.Fatalf("init program: %s", err)
		}
		program[i] = opcode
	}

	return
}

func insertNounAndVerb(noun, verb int, memory Memory) {
	memory[1], memory[2] = noun, verb
}

func (cpu *Processor) Run() {
	for {
		if cpu.Cycles > MAX_CYCLES {
			log.Fatalf("max cycles reached!!")
		}

		instruction := cpu.decode(cpu.Memory[cpu.IP])

		if instruction.Opcode == HCF {
			return
		}

		cpu.execute(instruction)

		cpu.IP += instruction.Size
		cpu.Cycles += 1
	}
}

func (cpu *Processor) decode(opcode int) Instruction {
	switch opcode {
	case 1:
		return InstructionAdd()
	case 2:
		return InstructionMultiply()
	case 99:
		return InstructionHalt()
	default:
		log.Fatalf("decode opcode %d", opcode)
		return InstructionHalt()
	}
}

func (cpu *Processor) execute(instruction Instruction) {
	switch instruction.Opcode {
	case ADD:
		cpu.execAdd()
	case MUL:
		cpu.execMul()
	}
}

func (cpu *Processor) execAdd() {
	addrLeft := cpu.Memory[cpu.IP+1]
	addrRight := cpu.Memory[cpu.IP+2]
	addrResult := cpu.Memory[cpu.IP+3]

	cpu.Memory[addrResult] = cpu.Memory[addrLeft] + cpu.Memory[addrRight]
}

func (cpu *Processor) execMul() {
	addrLeft := cpu.Memory[cpu.IP+1]
	addrRight := cpu.Memory[cpu.IP+2]
	addrResult := cpu.Memory[cpu.IP+3]

	cpu.Memory[addrResult] = cpu.Memory[addrLeft] * cpu.Memory[addrRight]
}
