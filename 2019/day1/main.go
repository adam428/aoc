package main

import (
	"log"
	"os"
	"strings"

	"github.com/AdamFromTumelo/aoc/2019/day1/mass"
)

func readFile(filepath string) (lines []string) {
	contents, err := os.ReadFile(filepath)
	if err != nil {
		log.Fatalf("reading %s: %s", filepath, err)
	}

	return strings.Split(strings.TrimSpace(string(contents)), "\n")

}

func main() {
	lines := readFile(`2019/day1/input.txt`)
	mass.SolvePart1(lines)
	mass.SolvePart2(lines)
}
