package mass

import (
	"log"
	"strconv"
)

func SolvePart1(lines []string) {
	sum := 0
	for _, line := range lines {

		num, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("parsing %s to int: %s", line, err)
		}

		sum += getFuelForMassPart1(num)
	}

	log.Printf("solution: %d", sum)
}

func getFuelForMassPart1(mass int) int {
	return (mass / 3) - 2
}

func SolvePart2(lines []string) {
	sum := 0
	for _, line := range lines {

		num, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("parsing %s to int: %s", line, err)
		}

		sum += getFuelForMassPart2(num)
	}

	log.Printf("solution: %d", sum)
}
func clampZero(n int) int {
	if n < 0 {
		return 0
	}
	return n
}

func getFuelForMassPart2(mass int) int {
	totalFuel := 0
	currentModule := mass
	for currentModule > 0 {
		currentFuel := clampZero(getFuelForMassPart1(currentModule))
		totalFuel += currentFuel
		currentModule = currentFuel
	}

	return totalFuel
}
