package mass

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_getFuelForMassPart1(t *testing.T) {

	tests := []struct {
		moduleMass int
		fuelMass   int
	}{
		{
			moduleMass: 12,
			fuelMass:   2,
		},
		{
			moduleMass: 14,
			fuelMass:   2,
		},
		{
			moduleMass: 1969,
			fuelMass:   654,
		},
		{
			moduleMass: 100756,
			fuelMass:   33583,
		},
	}
	for _, testCase := range tests {
		got := getFuelForMassPart1(testCase.moduleMass)
		want := testCase.fuelMass
		require.Equal(t, want, got)
	}
}
func Test_getFuelForMassPart2(t *testing.T) {

	tests := []struct {
		moduleMass int
		fuelMass   int
	}{
		{
			moduleMass: 12,
			fuelMass:   2,
		},
		{
			moduleMass: 14,
			fuelMass:   2,
		},
		{
			moduleMass: 1969,
			fuelMass:   966,
		},
		{
			moduleMass: 100756,
			fuelMass:   50346,
		},
	}
	for _, testCase := range tests {
		got := getFuelForMassPart2(testCase.moduleMass)
		want := testCase.fuelMass
		require.Equal(t, want, got)
	}
}
