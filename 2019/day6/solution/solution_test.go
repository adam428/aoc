package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_solution1(t *testing.T) {
	tests := []struct {
		Name  string
		Lines []string
		Want  int
	}{
		{
			Name: "simple - 1",
			Lines: []string{
				"COM)B",
			},
			Want: 1,
		},
		{
			Name: "simple - 2",
			Lines: []string{
				"COM)A",
				"COM)B",
			},
			Want: 2,
		},
		{
			Name: "simple - 4",
			Lines: []string{
				"COM)A",
				"COM)B",
				"B)D",
			},
			Want: 4,
		},
		{
			Name: "the example",
			Lines: []string{
				"COM)B",
				"B)C",
				"C)D",
				"D)E",
				"E)F",
				"B)G",
				"G)H",
				"D)I",
				"E)J",
				"J)K",
				"K)L",
			},
			Want: 42,
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.Name, func(t *testing.T) {
			require.Equal(t, testCase.Want, SolvePart1(testCase.Lines))
			require.Equal(t, 1, 1)
		})
	}
}

func Test_solution2(t *testing.T) {
	tests := []struct {
		Name  string
		Lines []string
		Want  int
	}{
		{
			Name: "simple - 1",
			Lines: []string{
				"COM)B",
				"COM)SAN",
				"B)YOU",
			},
			Want: 1,
		},
		{
			Name: "example",
			Lines: []string{
				"COM)B",
				"B)C",
				"C)D",
				"D)E",
				"E)F",
				"B)G",
				"G)H",
				"D)I",
				"E)J",
				"J)K",
				"K)L",
				"K)YOU",
				"I)SAN",
			},
			Want: 4,
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.Name, func(t *testing.T) {
			require.Equal(t, testCase.Want, SolvePart2(testCase.Lines))
			require.Equal(t, 1, 1)
		})
	}
}
