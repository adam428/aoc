package solution

import (
	"fmt"
	"log"
	"slices"
	"strings"

	"github.com/gammazero/deque"
)

func SolvePart1(lines []string) int {
	g := Graph{}
	g.Init()
	for _, line := range lines {
		parts := strings.Split(line, ")")
		if len(parts) != 2 {
			log.Fatalf("what's this input?! '%s'", line)
		}
		g.AddEdge(parts[0], parts[1])
	}

	result := 0
	for _, n := range g.GetNodes() {
		result += DFS_countPaths(&g, n.Name)
	}

	fmt.Printf("part1 >>>>>>>>> %d", result)
	return result
}

func SolvePart2(lines []string) int {
	g := Graph{}
	g.Init()
	for _, line := range lines {
		parts := strings.Split(line, ")")
		if len(parts) != 2 {
			log.Fatalf("what's this input?! '%s'", line)
		}
		g.AddEdge(parts[0], parts[1])
		g.AddEdge(parts[1], parts[0]) //bidi

	}

	you := g.GetNode("YOU").Neighbours[0]
	san := g.GetNode("SAN").Neighbours[0]
	result := shortestPath(&g, you.Name, san.Name)

	fmt.Printf("part2 >>>>>>>>> %d", result)
	return result
}

type Graph struct {
	NodeMap map[string]*Node
}

type Node struct {
	Name       string
	Neighbours []*Node
	Parent     string
}

func (g *Graph) Init() {
	g.NodeMap = make(map[string]*Node)
}

func (g *Graph) GetNodes() (nodes []*Node) {
	nodes = make([]*Node, len(g.NodeMap))
	i := 0
	for _, node := range g.NodeMap {
		nodes[i] = node
		i++
	}

	return nodes
}

func (g *Graph) AddEdge(a, b string) {
	aNode := g.AddNode(a)
	bNode := g.AddNode(b)

	aNode.Neighbours = append(aNode.Neighbours, bNode)
}

func (g *Graph) AddNode(name string) *Node {
	node := g.GetNode(name)
	g.NodeMap[name] = node
	return node
}

func (g *Graph) GetNode(name string) *Node {
	node, ok := g.NodeMap[name]
	if ok {
		return node
	}
	return &Node{
		Name: name,
	}
}

func initEmptyVisitedMap(g *Graph) map[string]bool {
	visited := make(map[string]bool, len(g.NodeMap))
	for k := range g.NodeMap {
		visited[k] = false
	}
	return visited
}

// DFS returns a list of paths starting at startingNodeName
func DFS_countPaths(g *Graph, startingNodeName string) int {
	visited := initEmptyVisitedMap(g)
	return dfs(g, startingNodeName, visited)
}

func dfs(g *Graph, startingNodeName string, visited map[string]bool) int {
	if visited[startingNodeName] {
		return 0
	}

	sum := 0
	node := g.GetNode(startingNodeName)
	for _, n := range node.Neighbours {
		sum += dfs(g, n.Name, visited) + 1
	}
	return sum
}

func shortestPath(g *Graph, from, to string) int {
	var queue deque.Deque[string]
	visited := initEmptyVisitedMap(g)

	queue.PushFront(from)
	for queue.Len() > 0 {
		v := queue.PopFront()
		if v == to {
			log.Printf("woo")
			break
		}
		neighbours := g.GetNode(v).Neighbours
		for _, neighbour := range neighbours {
			if visited[neighbour.Name] {
				continue
			}
			visited[neighbour.Name] = true
			neighbour.Parent = v
			queue.PushBack(neighbour.Name)
		}
	}

	p := g.GetNode(to).Parent
	iterations := 1
	for p != from && iterations < 100_000 {
		fmt.Println("> ", p)
		p = g.GetNode(p).Parent
		iterations += 1
	}

	return iterations
}

func (g *Graph) Dump() {
	com := g.NodeMap["COM"]
	g.dump(com, 0, []string{})
}

func (g *Graph) dump(node *Node, level int, visited []string) {
	if slices.Contains(visited, node.Name) {
		return
	}

	visited = append(visited, node.Name)

	for i := 0; i < level; i++ {
		fmt.Printf(" ")
	}
	fmt.Println(node.Name)

	for _, n := range node.Neighbours {
		g.dump(n, level+1, visited)
	}
}
