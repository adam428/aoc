package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_solution(t *testing.T) {

	tests := []struct {
		Name  string
		Input int
		Want  bool
	}{
		{
			Name:  "simple - 1",
			Input: 12234,
			Want:  true,
		},
	}
	for _, testCase := range tests {
		require.Equal(t, testCase.Want, ok(testCase.Input))
	}
}
