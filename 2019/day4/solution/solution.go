package solution

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func SolvePart1(start, end string) {

	n1, _ := strconv.Atoi(start)
	n2, _ := strconv.Atoi(end)

	total := 0
	for i := n1; i <= n2; i++ {
		if ok(i) {
			total += 1
		}
	}

	log.Println(" >>>>> solution: ", total)
}

func ok(i int) bool {
	return isMonotone(i) && hasTwoNeighbouringDigitsThatAreTheSame(i)
}

func hasTwoNeighbouringDigitsThatAreTheSame(i int) bool {
	s := fmt.Sprintf("%d", i)
	for x := 0; x < 10; x++ {
		re := regexp.MustCompile(fmt.Sprintf("%d+", x))
		re.Longest()

		matches := re.FindAll([]byte(s), -1)

		for _, m := range matches {
			if len(m) == 2 {
				log.Printf("`%v` %d %q\n", re, i, m)
				return true
			}
		}
	}

	return false
}

func isMonotone(i int) bool {
	chars := strings.Split(fmt.Sprintf("%d", i), "")
	for j := 1; j < len(chars); j++ {
		if chars[j-1] > chars[j] {
			return false
		}
	}
	return true
}
