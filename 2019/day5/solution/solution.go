package solution

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

func SolvePart1(lines []string) {
	program := initMemory(lines[0])
	processor := initProcessor(program)

	processor.Input = "1"
	processor.DieOnDump = true

	processor.Run()

	log.Println(" >>>>> solution: ", processor.Output)
}

func SolvePart2(lines []string) {
	program := initMemory(lines[0])
	processor := initProcessor(program)

	processor.Input = "5"
	processor.DieOnDump = true

	processor.Run()

	log.Println(" >>>>> solution: ", processor.Output)
}

type Memory []int

type Processor struct {
	IP     int
	Memory Memory
	Cycles int

	Input  string
	Output string

	DieOnDump bool
}

const MAX_CYCLES = 1_000_000

type Opcode int

const (
	ADD    Opcode = 1
	MUL    Opcode = 2
	INPUT  Opcode = 3
	OUTPUT Opcode = 4
	JMPT   Opcode = 5
	JMPF   Opcode = 6
	LT     Opcode = 7
	EQ     Opcode = 8
	HCF    Opcode = 99
	NIL    Opcode = 0
)

type ParameterMode int

const (
	PositionMode  ParameterMode = 0
	ImmediateMode ParameterMode = 1
)

type Instruction struct {
	Opcode         Opcode
	Size           int
	ParameterModes []ParameterMode
}

func decode(opcode Opcode) Instruction {
	switch opcode {
	case ADD:
		return Instruction{Opcode: ADD, Size: 4}
	case MUL:
		return Instruction{Opcode: MUL, Size: 4}
	case INPUT:
		return Instruction{Opcode: INPUT, Size: 2}
	case OUTPUT:
		return Instruction{Opcode: OUTPUT, Size: 2}
	case JMPT:
		return Instruction{Opcode: JMPT, Size: 3}
	case JMPF:
		return Instruction{Opcode: JMPF, Size: 3}
	case LT:
		return Instruction{Opcode: LT, Size: 4}
	case EQ:
		return Instruction{Opcode: EQ, Size: 4}
	case HCF:
		return Instruction{Opcode: HCF, Size: 1}
	}

	log.Fatalf("decoder: had trouble decoding %d", opcode)
	return Instruction{}
}

func initProcessor(program Memory) Processor {
	return Processor{
		Memory:    program,
		IP:        0,
		DieOnDump: false,
	}
}

func initMemory(line string) (program Memory) {
	opcodesStr := strings.Split(line, ",")
	program = make([]int, len(opcodesStr))

	for i, opcodeStr := range opcodesStr {
		opcode, err := strconv.Atoi(opcodeStr)
		if err != nil {
			log.Fatalf("init program: %s", err)
		}
		program[i] = opcode
	}

	return
}

func (cpu *Processor) Run() (output string) {
	for {
		if cpu.Cycles > MAX_CYCLES {
			log.Fatalf("max cycles reached!!")
		}

		instruction := cpu.decode(cpu.Memory[cpu.IP])

		if instruction.Opcode == HCF {
			return cpu.Output
		}

		oldIP := cpu.IP
		cpu.execute(instruction)

		// if the instruction modifies the instruction pointer,
		// that value is used and the instruction pointer is not automatically increased.
		if cpu.IP == oldIP {
			cpu.IP += instruction.Size
		}

		cpu.Cycles += 1
	}
}

func (cpu *Processor) decode(code int) Instruction {
	opcode := code % 100
	instruction := decode(Opcode(opcode))

	instruction = cpu.decodeParameterModes(code, instruction)
	return instruction
}

// ABCDE
// 01002
//
// DE - two-digit opcode,      02 == opcode 2
// C - mode of 1st parameter,  0 == position mode
// B - mode of 2nd parameter,  1 == immediate mode
// A - mode of 3rd parameter,  0 == position mode, omitted due to being a leading zero
func (cpu *Processor) decodeParameterModes(code int, instruction Instruction) Instruction {
	instruction.ParameterModes = make([]ParameterMode, instruction.Size-1)         //take 1 off for the opcode
	codeStr := fmt.Sprintf("%0"+fmt.Sprintf("%d", (instruction.Size+1))+"d", code) //pad with leading zeros to get a string of length opcode+num of parameters

	for i := 0; i < instruction.Size-1; i += 1 {
		currentParameterModeStr := string(codeStr[instruction.Size-2-i])
		switch string(currentParameterModeStr) {
		case "0":
			instruction.ParameterModes[i] = PositionMode
		case "1":
			instruction.ParameterModes[i] = ImmediateMode
		default:
			log.Fatalf("trouble decoding this: %d", code)
		}
	}

	return instruction
}

func (cpu *Processor) execute(instruction Instruction) {
	switch instruction.Opcode {
	case ADD:
		cpu.execAdd(instruction)
	case MUL:
		cpu.execMul(instruction)
	case INPUT:
		cpu.execInput(instruction)
	case OUTPUT:
		cpu.execOutput(instruction)
	case JMPT:
		cpu.execJumpIfTrue(instruction)
	case JMPF:
		cpu.execJumpIfFalse(instruction)
	case LT:
		cpu.execLessThan(instruction)
	case EQ:
		cpu.execEqual(instruction)
	}
}

func (cpu *Processor) loadValue(instruction Instruction, position int) int {
	mode := instruction.ParameterModes[position-1]
	switch mode {
	case ImmediateMode:
		return cpu.Memory[cpu.IP+position]
	case PositionMode:
		return cpu.Memory[cpu.Memory[cpu.IP+position]]
	default:
		log.Fatalf("loadValue: invalid parameter mode: %v", mode)
		return -99
	}
}

func (cpu *Processor) execAdd(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	cpu.Memory[cpu.Memory[cpu.IP+3]] = left + right
}

func (cpu *Processor) execMul(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	cpu.Memory[cpu.Memory[cpu.IP+3]] = left * right
}

func (cpu *Processor) execInput(instruction Instruction) {
	cpu.Memory[cpu.Memory[cpu.IP+1]] = cpu.readFromInput()
}

func (cpu *Processor) execOutput(instruction Instruction) {
	v := cpu.loadValue(instruction, 1)
	cpu.writeToOutput(v)
}

// Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the
// instruction pointer to the value from the second parameter.
// Otherwise, it does nothing.
func (cpu *Processor) execJumpIfTrue(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	if left != 0 {
		cpu.IP = right
	}
}

// Opcode 6 is jump-if-false: if the first parameter is zero, it sets the
// instruction pointer to the value from the second parameter.
// Otherwise, it does nothing.
func (cpu *Processor) execJumpIfFalse(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	if left == 0 {
		cpu.IP = right
	}
}

// Opcode 7 is less than: if the first parameter is less than the second
// parameter, it stores 1 in the position given by the third parameter.
// Otherwise, it stores 0.
func (cpu *Processor) execLessThan(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	var r int
	if left < right {
		r = 1
	} else {
		r = 0
	}
	cpu.Memory[cpu.Memory[cpu.IP+3]] = r
}

// Opcode 8 is equals: if the first parameter is equal to the second parameter,
// it stores 1 in the position given by the third parameter.
// Otherwise, it stores 0.
func (cpu *Processor) execEqual(instruction Instruction) {
	left := cpu.loadValue(instruction, 1)
	right := cpu.loadValue(instruction, 2)
	var r int
	if left == right {
		r = 1
	} else {
		r = 0
	}
	cpu.Memory[cpu.Memory[cpu.IP+3]] = r
}

func (cpu *Processor) readFromInput() int {
	lines := strings.SplitN(cpu.Input, "\n", 2)
	var line, remainder string

	if len(lines) > 1 {
		line, remainder = lines[0], lines[1]
	} else {
		line, remainder = lines[0], ""
	}

	if line == "" {
		log.Fatalf("out of inputs!")
	}

	cpu.Input = remainder
	n, err := strconv.Atoi(line)
	if err != nil {
		log.Fatalf("readFromInput: %v", err)
	}
	return n
}

func (cpu *Processor) writeToOutput(value int) {
	cpu.Output = fmt.Sprintf("%s%d\n", cpu.Output, value)
	log.Printf("=> %d\n", value)
}

func (cpu *Processor) dumpMemory(start, size int) {
	fmt.Println("---<~~ start ~~>---")
	for i := start; i < start+size; i++ {
		v := cpu.Memory[i]
		fmt.Printf("%04d | %04d", i, v)
		if cpu.IP == i {
			fmt.Printf(" <~~ IP")
		}
		fmt.Println()
		if i > size {
			break
		}
	}
	fmt.Println("---<~~ end ~~>---")
}
