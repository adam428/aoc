package main

import (
	"log"
	"os"
	"strings"

	"github.com/AdamFromTumelo/aoc/2019/day8/solution"
)

func readFile(filepath string) (lines []string) {
	contents, err := os.ReadFile(filepath)
	if err != nil {
		log.Fatalf("reading %s: %s", filepath, err)
	}

	return strings.Split(strings.TrimSpace(string(contents)), "\n")

}

func main() {
	input := readFile(`2019/day8/input.txt`)[0]
	solution.SolvePart1(input, 25, 6)
	solution.SolvePart2(input, 25, 6)
}
