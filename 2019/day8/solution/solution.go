package solution

import (
	"image"
	"image/color"
	"image/png"
	_ "image/png"
	"log"
	"os"
	"slices"
	"strings"
)

func SolvePart1(input string, width, height int) {
	layerSize := width * height

	layerZeros := make([]int, 0)
	layerResults := make([]int, 0)
	for i := 0; i < len(input); i += layerSize {
		layer := input[i : i+layerSize]

		zeroCount := strings.Count(layer, "0")
		oneCount := strings.Count(layer, "1")
		twoCount := strings.Count(layer, "2")

		layerZeros = append(layerZeros, zeroCount)
		layerResults = append(layerResults, oneCount*twoCount)
	}

	log.Println(" >>>>> solution: ", layerResults[slices.Index(layerZeros, slices.Min(layerZeros))])
}

const BLACK = '0'
const WHITE = '1'
const TRANSPARENT = '2'

func SolvePart2(input string, width, height int) {

	layerSize := width * height
	image := make([]byte, layerSize)
	// iterate over all the positions
	// for each position, go over each layer. ignore '2'. if '1' or '0', set in `image` and continue
	for position := 0; position < layerSize; position++ {
		for layerIndex := 0; layerIndex < len(input)/layerSize; layerIndex += 1 {
			// get current pixel in each layer:
			pixel := input[(layerIndex*layerSize)+position]
			image[position] = pixel
			if pixel != TRANSPARENT {
				break
			}

		}
	}

	drawThis(image, width, height)
}

func drawThis(bs []byte, width, height int) {
	im := image.NewGray(image.Rectangle{Max: image.Point{X: width, Y: height}})
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {

			p := bs[y*width+x]

			var g uint8
			if p == BLACK {
				g = 255
			}
			if p == WHITE {
				g = 20
			}
			if p == TRANSPARENT {
				g = 0
			}

			im.SetGray(x, y, color.Gray{
				Y: g,
			})
		}
	}

	f, err := os.Create("image.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, im); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
