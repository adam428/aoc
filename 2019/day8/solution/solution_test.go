package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_solution(t *testing.T) {

	tts := []struct {
		Name  string
		Input string
	}{
		{
			Name:  "simple - 1",
			Input: "123456789012",
		},
	}
	for _, tt := range tts {
		t.Run(tt.Name, func(t *testing.T) {
			SolvePart2(tt.Input, 3, 2)
			require.Equal(t, 0, 0)
		})
	}
}
