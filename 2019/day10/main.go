package main

import (
	"log"
	"os"
	"strings"

	"github.com/AdamFromTumelo/aoc/2019/day10/solution"
)

func readFile(filepath string) (lines []string) {
	contents, err := os.ReadFile(filepath)
	if err != nil {
		log.Fatalf("reading %s: %s", filepath, err)
	}

	return strings.Split(strings.TrimSpace(string(contents)), "\n")

}

func main() {
	input := readFile(`2019/day10/input.txt`)
	solution.SolvePart1(input)
	solution.SolvePart2(input)
}
