package solution

import (
	"fmt"
	"log"
	"math"
)

func SolvePart1(input []string) {
	var d Data
	d.Init(input)
	log.Println(" >>>>> solution: ", d.BestAsteroidScore())
}

func SolvePart2(input []string) {

	// log.Println(" >>>>> solution: ")
}

type Data struct {
	asteroids []Point
}

type Point struct {
	x, y int
}

func (d *Data) scoreForOurGuy(ourGuy Point) (score int) {
	// theta = atan(oy - ty / ox - tx)
	others := make(map[float64][]Point)
	for _, p := range d.asteroids {
		if p == ourGuy {
			continue
		}

		theta := math.Atan2(
			float64(ourGuy.y)-float64(p.y),
			float64(ourGuy.x)-float64(p.x),
		)
		others[theta] = append(others[theta], p)
	}

	return len(others)
}

// func distance(a, b Point) float64 {
// 	return math.Sqrt(
// 		math.Pow(float64(a.x)-float64(b.x), 2) +
// 			math.Pow(float64(a.y)-float64(b.y), 2),
// 	)
// }

func (d *Data) BestAsteroidScore() (maxScore int) {
	for _, origin := range d.asteroids {
		score := d.scoreForOurGuy(origin)
		if score > maxScore {
			maxScore = score
			fmt.Println("new max: ", origin, score)
		}
	}

	return maxScore
}

func (d *Data) Init(input []string) {
	for y, line := range input {
		for x, char := range line {
			p := Point{x, y}
			if char == '#' {
				d.asteroids = append(d.asteroids, p)
			}
		}
	}
}
