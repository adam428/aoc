package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_solution(t *testing.T) {

	tests := []struct {
		Name  string
		Input string
		Want  []Coordinate
	}{
		{
			Name:  "simple - 1",
			Input: "R1",
			Want: []Coordinate{
				{X: 1, Y: 0},
			},
		},
		{
			Name:  "simple - 2",
			Input: "R1,U1",
			Want: []Coordinate{
				{X: 1, Y: 0},
				{X: 1, Y: 1},
			},
		},
		{
			Name:  "simple - 3",
			Input: "R1,U1,L1,D1",
			Want: []Coordinate{
				{X: 1, Y: 0},
				{X: 1, Y: 1},
				{X: 0, Y: 1},
				{X: 0, Y: 0},
			},
		},
	}
	for _, testCase := range tests {
		movements := loadMovements(testCase.Input)
		got := expandMovementsToPath(movements)

		require.Equal(t, testCase.Want, got, testCase.Name)
	}
}
