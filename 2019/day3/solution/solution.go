package solution

import (
	"log"
	"math"
	"strconv"
	"strings"
)

func SolvePart1(lines []string) {
	log.Println(" >>>>> solution: ", "??")

	example1 := "R75,D30,R83,U83,L12,D49,R71,U7,L72"
	example2 := "U62,R66,U55,R34,D71,R55,D58,R83"

	path1 := expandMovementsToPath(loadMovements(example1))
	path2 := expandMovementsToPath(loadMovements(example2))

	log.Println(closestIntersectionByManhattan(path1, path2))

	example3 := "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
	example4 := "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"

	path3 := expandMovementsToPath(loadMovements(example3))
	path4 := expandMovementsToPath(loadMovements(example4))

	log.Println(closestIntersectionByManhattan(path3, path4))

	path5 := expandMovementsToPath(loadMovements(lines[0]))
	path6 := expandMovementsToPath(loadMovements(lines[1]))

	// log.Println(closestIntersectionByManhattan(path5, path6))
	log.Println(closestIntersectionBySteps(path5, path6))

}

func SolvePart2(lines []string) {}

type Direction string

const (
	UP    Direction = "U"
	DOWN  Direction = "D"
	RIGHT Direction = "R"
	LEFT  Direction = "L"
)

type Movement struct {
	Direction Direction
	Distance  int
}

func loadMovements(line string) (movements []Movement) {
	movementsStr := strings.Split(line, ",")
	movements = make([]Movement, len(movementsStr))
	for i, movementStr := range movementsStr {
		movements[i] = mapStrToMovement(movementStr)
	}
	return
}

func mapStrToMovement(movStr string) Movement {
	var direction Direction
	if strings.HasPrefix(movStr, string(LEFT)) {
		direction = LEFT
	}
	if strings.HasPrefix(movStr, string(UP)) {
		direction = UP
	}
	if strings.HasPrefix(movStr, string(RIGHT)) {
		direction = RIGHT
	}
	if strings.HasPrefix(movStr, string(DOWN)) {
		direction = DOWN
	}

	distance, err := strconv.Atoi(movStr[1:])
	if err != nil {
		log.Fatalf("converting %s to distance: ", err)
	}

	return Movement{
		Direction: direction,
		Distance:  distance,
	}
}

type Coordinate struct {
	X int
	Y int
}

func expandMovementsToPath(movements []Movement) (path []Coordinate) {
	coordinate := Coordinate{} //origin
	for _, movement := range movements {
		wake := wake(movement, coordinate)
		path = append(path, wake...)
		coordinate = path[len(path)-1]
	}

	return
}

func wake(movement Movement, startingCoordinate Coordinate) (wake []Coordinate) {
	x := startingCoordinate.X
	y := startingCoordinate.Y

	if movement.Direction == LEFT {
		for x = startingCoordinate.X; x >= startingCoordinate.X-movement.Distance; x -= 1 {
			wake = append(wake, Coordinate{X: x, Y: y})
		}
	} else if movement.Direction == RIGHT {
		for x = startingCoordinate.X; x <= startingCoordinate.X+movement.Distance; x += 1 {
			wake = append(wake, Coordinate{X: x, Y: y})
		}
	} else if movement.Direction == DOWN {
		for y = startingCoordinate.Y; y >= startingCoordinate.Y-movement.Distance; y -= 1 {
			wake = append(wake, Coordinate{X: x, Y: y})
		}
	} else if movement.Direction == UP {
		for y = startingCoordinate.Y; y <= startingCoordinate.Y+movement.Distance; y += 1 {
			wake = append(wake, Coordinate{X: x, Y: y})
		}
	}

	return wake[1:]
}

func findIntersections(path1, path2 []Coordinate) (intersections []Coordinate) {
	for _, c1 := range path1 {
		for _, c2 := range path2 {
			if c1 == c2 {
				intersections = append(intersections, c1)
			}
		}
	}

	return
}

func manhattan(coordinate Coordinate) int {
	return int(math.Abs(float64(coordinate.X)) + math.Abs(float64(coordinate.Y)))
}

func closestIntersectionByManhattan(path1, path2 []Coordinate) (min int) {
	ds := []int{}
	intersections := findIntersections(path1, path2)
	for _, intersection := range intersections {
		ds = append(ds, manhattan(intersection))
	}

	min = ds[0]
	for _, d := range ds {
		if min > d {
			min = d
		}
	}

	return
}

func steps(path []Coordinate, target Coordinate) int {
	for i, c := range path {
		if c == target {
			return i + 1
		}
	}

	log.Fatal("oops not found target on path ", target)
	return len(path)
}

func closestIntersectionBySteps(path1, path2 []Coordinate) (min int) {
	ds := []int{}
	intersections := findIntersections(path1, path2)
	for _, intersection := range intersections {
		d1 := steps(path1, intersection)
		d2 := steps(path2, intersection)
		ds = append(ds, d1+d2)
	}

	min = ds[0]
	for _, d := range ds {
		if min > d {
			min = d
		}
	}

	return
}
