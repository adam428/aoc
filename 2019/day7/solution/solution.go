package solution

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/AdamFromTumelo/aoc/2019/day7/intcode"
	"golang.org/x/exp/slices"
)

func SolvePart1(lines []string) {
	amplifierControllerProgram := intcode.InitMemory(lines[0])

	amps := []intcode.Processor{
		intcode.InitProcessor(amplifierControllerProgram),
		intcode.InitProcessor(amplifierControllerProgram),
		intcode.InitProcessor(amplifierControllerProgram),
		intcode.InitProcessor(amplifierControllerProgram),
		intcode.InitProcessor(amplifierControllerProgram),
	}

	phaseSettings := []int{0, 1, 2, 3, 4}
	phaseSquences := Permutations(phaseSettings)
	outputs := []int{}
	for _, sequence := range phaseSquences {
		lastOutput := 0
		for i, amp := range amps {
			amp.Input = fmt.Sprintf("%d\n%d\n", sequence[i], lastOutput)

			amp.Run()
			var err error
			lastOutput, err = strconv.Atoi(strings.Trim(amp.Output, "\n"))
			if err != nil {
				log.Fatalf("aiiii.... %v", err)
			}
		}

		fmt.Printf("%v -> %d", sequence, lastOutput)
		outputs = append(outputs, lastOutput)
	}
	log.Println()
	log.Println()
	log.Println()
	log.Println()
	slices.Sort(outputs)
	log.Println(outputs)
}

func next(i, l int) int {
	return (i + 1) % l
}
func prev(i, l int) int {
	return (i + l - 1) % l
}

func areSomeAmpsRunning(amps []intcode.Processor) bool {
	for _, p := range amps {
		if !p.Halted {
			return true
		}
	}

	return false
}

func SolvePart2(lines []string) int {
	amplifierControllerProgram := intcode.InitMemory(lines[0])

	phaseSettings := []int{9, 8, 7, 6, 5}
	ampEOutputs := []int{}
	// for _, sequence := range [][]int{phaseSettings} {
	for _, sequence := range Permutations(phaseSettings) {
		amps := []intcode.Processor{
			intcode.InitProcessor(amplifierControllerProgram),
			intcode.InitProcessor(amplifierControllerProgram),
			intcode.InitProcessor(amplifierControllerProgram),
			intcode.InitProcessor(amplifierControllerProgram),
			intcode.InitProcessor(amplifierControllerProgram),
		}
		for i := range amps {
			amps[i].EnterInput(sequence[i])
		}

		outputs := []int{0, 0, 0, 0, 0}
		maxIterations := 1_000
		for i := 0; areSomeAmpsRunning(amps); i = next(i, len(amps)) {
			if amps[i].Halted {
				continue
			}
			maxIterations -= 1
			if maxIterations <= 0 {
				log.Fatalf("time out")
			}

			previousAmpIndex := prev(i, len(amps))
			input := outputs[previousAmpIndex]
			amps[i].EnterInput(input)

			amps[i].Run()
			amps[i].Input = ""

			output := amps[i].PopOutput()
			outputInt, err := strconv.Atoi(strings.TrimSpace(output))
			if err != nil {
				log.Printf("getting output from amp%d: %v", i, err)
			}
			outputs[i] = outputInt
			// log.Printf("%v: amp%d: %q > %d", sequence, i, wholeInput, outputs[i])

			if amps[i].Halted {
				log.Printf("%v: amp%d: halted: %d", sequence, i, outputs[i])
			}
		}

		ampEOutputs = append(ampEOutputs, outputs[len(amps)-1])
	}

	fmt.Println(">>>>>>>>>>>>>> ", slices.Max(ampEOutputs))
	return slices.Max(ampEOutputs)
}
