package solution

import (
	"fmt"
	"testing"

	"gotest.tools/assert"
)

func Test_Permutations_int(t *testing.T) {

	slice := []int{1, 2, 3}
	allPermutations := Permutations(slice)
	for _, perm := range allPermutations {
		fmt.Println(perm)
	}
	assert.Equal(t, len(allPermutations), 6)
}

func Test_Permutations_string(t *testing.T) {

	slice := []string{"aa", "bb", "cc"}
	allPermutations := Permutations(slice)
	for _, perm := range allPermutations {
		fmt.Println(perm)
	}
	assert.Equal(t, len(allPermutations), 6)
}
