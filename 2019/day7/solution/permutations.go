package solution

import "golang.org/x/exp/slices"

func Permutations[T any](arr []T) [][]T {
	if len(arr) == 0 {
		return [][]T{}
	}
	if len(arr) == 1 {
		return [][]T{arr}
	}

	var result [][]T

	for i, v := range arr {
		rest := slices.Delete(slices.Clone(arr), i, i+1)
		for _, perm := range Permutations(rest) {
			result = append(result, append([]T{v}, perm...))
		}
	}

	return result
}
