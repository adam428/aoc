package solution

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_Solution2(t *testing.T) {

	tts := []struct {
		name   string
		code   string
		result int
	}{
		{
			name:   "example1",
			code:   "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5",
			result: 139629729,
		},
		{
			name:   "example2",
			code:   "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10",
			result: 18216,
		},
	}

	for _, tt := range tts {
		t.Run(tt.name, func(t *testing.T) {
			result := SolvePart2([]string{tt.code})
			require.Equal(t, tt.result, result)
		})
	}
}

func Test_prev(t *testing.T) {
	require.Equal(t, 5, prev(0, 6))

}
