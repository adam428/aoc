package main

import (
	"log"
	"os"
	"strings"

	"github.com/AdamFromTumelo/aoc/2019/day7/solution"
)

func readFile(filepath string) (lines []string) {
	contents, err := os.ReadFile(filepath)
	if err != nil {
		log.Fatalf("reading %s: %s", filepath, err)
	}

	return strings.Split(strings.TrimSpace(string(contents)), "\n")

}

func main() {
	lines := readFile(`2019/day7/input.txt`)
	solution.SolvePart1(lines)
	solution.SolvePart2(lines)
}
