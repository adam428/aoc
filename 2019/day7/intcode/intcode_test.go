package intcode

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_firstBatch(t *testing.T) {
	tests := []struct {
		Name          string
		Code          string
		Input         string
		FinishedState []int
	}{
		{
			Name:          "simple",
			Code:          "1101,100,-1,4,0",
			FinishedState: []int{1101, 100, -1, 4, 99},
		},
		{
			Name:          "simple 2",
			Code:          "1102,9,11,4,0",
			FinishedState: []int{1102, 9, 11, 4, 99},
		},
		{
			Name:          "simple 3",
			Code:          "1002,4,3,4,33",
			FinishedState: []int{1002, 4, 3, 4, 99},
		},

		{
			Name:          "input",
			Code:          "3,0,4,0,99",
			Input:         "55",
			FinishedState: []int{55, 0, 4, 0, 99},
		},

		{
			Name:          "olden: simple",
			Code:          "1,0,0,0,99",
			FinishedState: []int{2, 0, 0, 0, 99},
		},
		{
			Name:          "olden: simple 2",
			Code:          "2,3,0,3,99",
			FinishedState: []int{2, 3, 0, 6, 99},
		},
		{
			Name:          "olden: not so simple",
			Code:          "2,4,4,5,99,0",
			FinishedState: []int{2, 4, 4, 5, 99, 9801},
		},
		{
			Name:          "olden: last",
			Code:          "1,1,1,4,99,5,6,0,99",
			FinishedState: []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.Name, func(t *testing.T) {
			memory := InitMemory(testCase.Code)
			processor := InitProcessor(memory)
			processor.Input = testCase.Input
			processor.Run()
			got := processor.Memory
			want := testCase.FinishedState
			require.Equal(t, []int(want), []int(got))
			require.Equal(t, 1, 1)
		})
	}
}

func Test_nextbatch(t *testing.T) {
	tests := []struct {
		Name          string
		Code          string
		Input         string
		Output        string
		FinishedState []int
	}{
		{
			Name:          "EQ: Using position mode, consider whether the input is equal to 8; output 1 (if it is).",
			Code:          "3,9,8,9,10,9,4,9,99,-1,8",
			Input:         "8",
			Output:        "1",
			FinishedState: []int{3, 9, 8, 9, 10, 9, 4, 9, 99, 1, 8},
		},
		{
			Name:          "EQ: Using position mode, consider whether the input is equal to 8; output 0 (if it is not).",
			Code:          "3,9,8,9,10,9,4,9,99,-1,8",
			Input:         "1",
			Output:        "0",
			FinishedState: []int{3, 9, 8, 9, 10, 9, 4, 9, 99, 0, 8},
		},
		{
			Name:          "EQ: Using immediate mode, consider whether the input is equal to 8; output 1 (if it is).",
			Code:          "3,3,1108,-1,8,3,4,3,99",
			Input:         "8",
			Output:        "1",
			FinishedState: []int{3, 3, 1108, 1, 8, 3, 4, 3, 99},
		},
		{
			Name:          "EQ: Using immediate mode, consider whether the input is equal to 8; output 0 (if it is not).",
			Code:          "3,3,1108,-1,8,3,4,3,99",
			Input:         "1",
			Output:        "0",
			FinishedState: []int{3, 3, 1108, 0, 8, 3, 4, 3, 99},
		},
		{
			Name:          "LT: position; true",
			Code:          "3,9,7,9,10,9,4,9,99,-1,8",
			Input:         "7",
			Output:        "1",
			FinishedState: []int{3, 9, 7, 9, 10, 9, 4, 9, 99, 1, 8},
		},
		{
			Name:          "LT: position; false",
			Code:          "3,9,7,9,10,9,4,9,99,-1,8",
			Input:         "9",
			Output:        "0",
			FinishedState: []int{3, 9, 7, 9, 10, 9, 4, 9, 99, 0, 8},
		},
		{
			Name:          "LT: immediate; true",
			Code:          "3,3,1107,-1,8,3,4,3,99",
			Input:         "7",
			Output:        "1",
			FinishedState: []int{3, 3, 1107, 1, 8, 3, 4, 3, 99},
		},
		{
			Name:          "LT: immediate; false",
			Code:          "3,3,1107,-1,8,3,4,3,99",
			Input:         "9",
			Output:        "0",
			FinishedState: []int{3, 3, 1107, 0, 8, 3, 4, 3, 99},
		},
		{
			Name:          "JMP: position: 0",
			Code:          "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
			Input:         "0",
			Output:        "0",
			FinishedState: []int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, 0, 0, 1, 9},
		},
		{
			Name:          "JMP: immediate: 0",
			Code:          "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
			Input:         "0",
			Output:        "0",
			FinishedState: []int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, 0, 0, 1, 9},
		},
		{
			Name:          "JMP: position: 1",
			Code:          "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
			Input:         "2",
			Output:        "1",
			FinishedState: []int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, 2, 1, 1, 9},
		},
		{
			Name:          "JMP: immediate: 1",
			Code:          "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
			Input:         "2",
			Output:        "1",
			FinishedState: []int{3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, 2, 1, 1, 9},
		},
		{
			Name:   "big ol example - below 8",
			Code:   "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
			Input:  "0",
			Output: "999",
		},
		{
			Name:   "big ol example - exactly 8",
			Code:   "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
			Input:  "8",
			Output: "1000",
		},
		{
			Name:   "big ol example - above 8",
			Code:   "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
			Input:  "100",
			Output: "1001",
		},
	}

	for _, testCase := range tests {
		t.Run(testCase.Name, func(t *testing.T) {
			memory := InitMemory(testCase.Code)
			processor := InitProcessor(memory)
			processor.Input = testCase.Input
			processor.Run()
			got := processor.Memory
			want := testCase.FinishedState
			if len(want) != 0 {
				require.Equal(t, []int(want), []int(got))
			}
			require.Equal(t, testCase.Output, processor.Output)
		})
	}
}

func Test_blockForInput(t *testing.T) {
	code := "3,0,99"
	input := "1"
	blockingState := []int{3, 0, 99}
	finishedState := []int{1, 0, 99}

	processor := InitProcessor(InitMemory(code))
	processor.Input = ""
	processor.Run()
	got := processor.Memory
	want := blockingState
	require.Equal(t, []int(want), []int(got))

	processor.Input = input
	processor.Run()

	got = processor.Memory
	want = finishedState
	require.Equal(t, []int(want), []int(got))
}
